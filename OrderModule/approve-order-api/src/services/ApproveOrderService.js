/* eslint-disable no-unused-vars */
const Service = require('./Service');

const ObjectID = require('mongodb').ObjectID;
const MongoClient = require('mongodb').MongoClient;

const axios = require('axios');

function getApproveOrderDatabaseCollection(collectionName) {
  return new Promise(async function(resolve) {
    const connection = await MongoClient.connect('mongodb://db_approve_order');
    const db = await connection.db('approve-order');
    const collection = await db.collection(collectionName);
    resolve(collection);
  });
}

async function getPageOfDocuments(collection, sortBy, limit, pagingToken, filterQuery={}) {
  var upOrDown = -1;
  var op = '$lt';
  var defaultPagingToken = 'f'.repeat(24);
  if (sortBy === '+created') {
    upOrDown = 1;
    op = '$gt';
    defaultPagingToken = '0'.repeat(24);
  }
  if ( (typeof pagingToken) !== 'string' || pagingToken === '') {
    pagingToken = defaultPagingToken;
  }
  
  const pagingQuery = {_id: {[op]: ObjectID(pagingToken) }};
  const query = {...pagingQuery, ...filterQuery};
  const sortOrder = {_id: upOrDown};
  const cursor = await collection.find(query).sort(sortOrder).limit(limit);
  const documents = await cursor.toArray();
  let nextPagingToken = '';
  if (documents.length > 0) {
    nextPagingToken = documents[documents.length-1]['_id'];
  }
  return {
    pagingToken: nextPagingToken,
    [collection.collectionName]: documents
  };
}

async function foundOrderAndModifiedStatus(collection, id, status) {
  const response = await collection.updateOne(
    {_id: ObjectID(id)},
    {$set: {reviewStatus: status}}
  );
  // response from updateOne includes a result object with a field n
  // n indicates the number of documents updated
  return response.result.n > 0;
}

/**
* Approve an order
*
* id String id of order to approve
* returns OrderResponse
* */
const approveOrder = ({ id }) => new Promise(
  async (resolve, reject) => {
    try {
      const collection = await getApproveOrderDatabaseCollection('orders');
      const success = foundOrderAndModifiedStatus(collection, id, 'approved'); 
      if (success) {
        const order = await collection.findOne({'_id': ObjectID(id)});
        resolve(Service.successResponse({
          order,
        }));
      } else {
        throw {
          message: "No such order.",
          status: 404
        }
      }
    } catch (e) {
      reject(Service.rejectResponse(
        e.message || 'Invalid input',
        e.status || 405,
      ));
    }
  },
);

/**
* List all approved orders
*
* sortBy String Which field to sort by (optional)
* limit Integer Number of items to return (optional)
* pagingToken String Used to specify next page (optional)
* returns List
* */
const getApprovedOrders = ({ sortBy, limit, pagingToken }) => new Promise(
  async (resolve, reject) => {
    try {
      const collection = await getApproveOrderDatabaseCollection('orders');
      const approved = await getPageOfDocuments(collection, sortBy, limit, pagingToken, {reviewStatus: 'approved'});     
      resolve(Service.successResponse(approved));
    } catch (e) {
      reject(Service.rejectResponse(
        e.message || 'Invalid input',
        e.status || 405,
      ));
    }
  },
);

/**
* List all unreviewed orders
*
* sortBy String Which field to sort by (optional)
* limit Integer Number of items to return (optional)
* pagingToken String Used to specify next page (optional)
* returns List
* */
const getUnreviewedOrders = ({ sortBy, limit, pagingToken }) => new Promise(
  async (resolve, reject) => {
    try {
      // Get most recently seen order.
      const collection = await getApproveOrderDatabaseCollection('orders');
      const cur = await collection.find().sort({_id: -1}).limit(1);
      const array = await cur.toArray();

      let uri = "http://api_place_order:3000/v1/orders?sortBy=%2Bcreated";
      let token = array.length > 0 ? array[0]._id.toString() : '';

      let response = null;
      let orders = null;
      do {
        console.log(uri+ (token.length > 0 ? `&pagingToken=${token}` : ''));
        response = await axios.get(uri+ (token.length > 0 ? `&pagingToken=${token}` : ''));
        orders = response.data["orders"];
        orders.forEach(item => {
          token = item._id;
          item._id = ObjectID(item._id);
          item.reviewStatus = "unreviewed";
        });
        if (orders.length > 0) {
          await collection.insertMany(orders);
        }
      } while (orders.length > 0);

      const unreviewed = await getPageOfDocuments(collection, sortBy, limit, pagingToken, {reviewStatus: 'unreviewed'});
     
      resolve(Service.successResponse(unreviewed));
    } catch (e) {
      reject(Service.rejectResponse(
        e.message || 'Invalid input',
        e.status || 405,
      ));
    }
  },
);

/**
* cancel an order
*
* id String id of order to cancel
* returns OrderResponse
* */
const cancelOrder = ({ id }) => new Promise(
  async (resolve, reject) => {
    try {
      const collection = await getApproveOrderDatabaseCollection('orders');
      const success = foundOrderAndModifiedStatus(collection, id, 'cancelled'); 
      if (success) {
        const order = await collection.findOne({'_id': ObjectID(id)});
        resolve(Service.successResponse({
          order,
        }));
      } else {
        throw {
          message: "No such order.",
          status: 404
        }
      }
    } catch (e) {
      reject(Service.rejectResponse(
        e.message || 'Invalid input',
        e.status || 405,
      ));
    }
  },
);

module.exports = {
  approveOrder,
  getApprovedOrders,
  getUnreviewedOrders,
  cancelOrder,
};
